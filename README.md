# Missionaries and cannibals problem

An A* algorithm to solve the missionaries and cannibals problem.

You can find the problem statement here : https://en.wikipedia.org/wiki/Missionaries_and_cannibals_problem

The output is a list of moves describing the solution, in the following form :

(missionaries on left side, cannibals on left side, boat position ("L" or "R"), missionaries on right side, cannibals on right side)