from queue import PriorityQueue
from math import inf

# state : (missionaries on left side, cannibals on left side, boat position ("L" or "R"),
#          missionaries on right side, cannibals on right side)


def get_next_states(state):
    """Return the list of the next possible states"""
    (left_missionaries, left_cannibals, boat_position, right_missionaries, right_cannibals) = state
    next_states = []
    if boat_position == "L":
        missionaries_to_take = left_missionaries
        cannibals_to_take = left_cannibals
    else:
        missionaries_to_take = right_missionaries
        cannibals_to_take = right_cannibals
    # we can take from 0 to 2 people
    for taken_missionaries in range(0, min(2, missionaries_to_take) + 1):
        for taken_cannibals in range(0, min(cannibals_to_take, 2 - taken_missionaries) + 1):
            if taken_cannibals + taken_missionaries > 0:
                if boat_position == "L":
                    next_states.append((left_missionaries - taken_missionaries, left_cannibals - taken_cannibals,
                                        "R", right_missionaries + taken_missionaries, right_cannibals + taken_cannibals))
                else:
                    next_states.append((left_missionaries + taken_missionaries, left_cannibals + taken_cannibals,
                                        "L", right_missionaries - taken_missionaries, right_cannibals - taken_cannibals))
    return next_states


def goal(state):
    """Check if it is finished (everyone on the right side with the boat)"""
    (left_missionaries, left_cannibals, boat_position, right_missionaries, right_cannibals) = state
    return right_missionaries == 3 and right_cannibals == 3 and boat_position == "R"


def cost(state):
    """Compute the cost of the state (infinite if invalid, else 0)"""
    (left_missionaries, left_cannibals, boat_position, right_missionaries, right_cannibals) = state
    if 0 < left_missionaries < left_cannibals or 0 < right_missionaries < right_cannibals:
        return inf
    else:
        return 1


def heuristic(state):
    """Heuristic for the A-star algorithm : here, we chose the number of people on the left side"""
    (left_missionaries, left_cannibals, boat_position, right_missionaries, right_cannibals) = state
    return left_missionaries + left_cannibals


def get_path(came_from):
    """Reconstruct the path from the "came_from" dictionary"""
    current_state = (0, 0, "R", 3, 3)
    path = []
    while current_state != (3, 3, "L", 0, 0):
        path.insert(0, current_state)
        current_state = came_from[current_state]
    return path


# A-star algorithm
node = (3, 3, "L", 0, 0)
frontier = PriorityQueue()
frontier.put((0, node))
came_from = {node: None}
cost_so_far = {node: 0}
current_state = None

while not frontier.empty() and (current_state is None or not goal(current_state)):
    current_state = frontier.get()[1]
    if not goal(current_state):
        for next_state in get_next_states(current_state):
            new_cost = cost_so_far[current_state] + cost(next_state)
            if next_state not in cost_so_far or new_cost < cost_so_far[next_state]:
                cost_so_far[next_state] = new_cost
                priority = new_cost + heuristic(next_state)
                frontier.put((priority, next_state))
                came_from[next_state] = current_state

print(get_path(came_from))

